//
//  TitleView.swift
//  ChartTest
//
//  Created by Alex Bofu on 7/2/17.
//  Copyright © 2017 Alex Bofu. All rights reserved.
//

import UIKit

class TitleView: UIView {

    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var subtitleLabel: UILabel!
    
    //MARK: - Public
    class func getTitleView(for title: String, subtitle: String = "", isSecondaryLableHidden: Bool = true) -> TitleView {
        let titleView: TitleView = UIView.fromNib()
        titleView.titleLabel.text = title
        
        if !isSecondaryLableHidden {
            titleView.subtitleLabel.text = subtitle
        }
        
        return titleView
    }
}
