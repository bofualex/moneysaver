//
//  MoneyTableViewCell.swift
//  ChartTest
//
//  Created by Alex Bofu on 6/26/17.
//  Copyright © 2017 Alex Bofu. All rights reserved.
//

import UIKit

class MoneyTableViewCell: UITableViewCell {

    @IBOutlet weak var expenseTypeImageView: UIImageView!
    @IBOutlet weak var expenseTypeLabel: UILabel!
    @IBOutlet weak var expenseTotalLabel: UILabel!

    func setupCell(for expenseType: ExpenseButtonTuple) {
        expenseTypeImageView.image = UIImage(named: expenseType.image)
        expenseTypeLabel.text = expenseType.name
    }
}
