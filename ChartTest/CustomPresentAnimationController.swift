//
//  CustomPresentAnimationController.swift
//  CustomTransitions
//
//  Created by Joyce Echessa on 3/3/15.
//  Copyright (c) 2015 Appcoda. All rights reserved.
//

import UIKit

class CustomPresentAnimationController: NSObject, UIViewControllerAnimatedTransitioning {
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 0.7
    }
    
//    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
//        
//        let fromViewController = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.from)!
//        let toViewController = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.to)!
//        let finalFrameForVC = transitionContext.finalFrame(for: toViewController)
//        let containerView = transitionContext.containerView
//        let bounds = UIScreen.main.bounds
//        toViewController.view.frame = finalFrameForVC.offsetBy(dx: 0, dy: -bounds.size.height)
//        containerView.addSubview(toViewController.view)
//        
//        UIView.animate(withDuration: transitionDuration(using: transitionContext), delay: 0.0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.1, options: .curveEaseIn, animations: {
//            fromViewController.view.alpha = 0.8
//            toViewController.view.frame = finalFrameForVC
//            }, completion: {
//                finished in
//                transitionContext.completeTransition(true)
//                fromViewController.view.alpha = 1.0
//        })
//    }
//    
//    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
////        let fromViewController = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.from)!
//        let toViewController = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.to)!
//        let finalFrameForVC = transitionContext.finalFrame(for: toViewController)
//        let containerView = transitionContext.containerView
//        let bounds = UIScreen.main.bounds
//        
//        let rect = CGRect(x: bounds.origin.x, y: bounds.origin.y, width: 0, height: 0)
//        toViewController.view.frame = rect
////        toViewController.view.frame = finalFrameForVC.offsetBy(dx: -bounds.size.width, dy: -bounds.size.height)
//        containerView.addSubview(toViewController.view)
////        containerView.sendSubview(toBack: toViewController.view)
//        toViewController.view.alpha = 0.5
//
//        UIView.animate(withDuration: transitionDuration(using: transitionContext), animations: {
//            toViewController.view.frame = finalFrameForVC
////            fromViewController.view.frame = fromViewController.view.frame.insetBy(dx: fromViewController.view.frame.size.width / 2, dy: fromViewController.view.frame.size.height / 2)
//            toViewController.view.alpha = 1
//        }, completion: {
//            finished in
//            transitionContext.completeTransition(true)
////            toViewController.view.alpha = 1
//        })
//    }

    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        let fromViewController = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.from)!
        let toViewController = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.to)!
        let finalFrameForVC = transitionContext.finalFrame(for: toViewController)
        let containerView = transitionContext.containerView
        
        let bounds = UIScreen.main.bounds
        let rect = CGRect(x: bounds.width / 2, y: bounds.height / 2, width: 80, height: 80)

        toViewController.view.frame = rect
        toViewController.view.alpha = 0.5

        containerView.addSubview(toViewController.view)
        
        UIView.animate(withDuration: transitionDuration(using: transitionContext), animations: {
            toViewController.view.frame = finalFrameForVC
            toViewController.view.alpha = 1
        }, completion: {
            finished in
            fromViewController.view.removeFromSuperview()
            transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
        })
    }

}
