//
//  ViewController2.swift
//  ChartTest
//
//  Created by Alex Bofu on 7/2/17.
//  Copyright © 2017 Alex Bofu. All rights reserved.
//

import UIKit
import MapKit

class ViewController2: UIViewController {
    
    @IBOutlet var placesMapView: MKMapView!
    @IBOutlet var placesTableView: UITableView!
    
    fileprivate var tableHeaderView: TableHeaderView?
    fileprivate var titleView: TitleView?
    fileprivate let customPresentAnimationController = CustomPresentAnimationController()
    fileprivate let customDismissAnimationController = CustomDismissAnimationController()
    
    fileprivate var coordinateArray: [(lat: Double, long: Double)] = [(51.509865, -0.118092), (51.510865, -0.116092), (51.509165, -0.110092), (51.512865, -0.110092), (51.511865, -0.120092), (51.520865, -0.110092), (51.500865, -0.109092), (51.507865, -0.121092), (51.509865, -0.117092)]
    fileprivate var placesNamesArray = ["(Plimstock Street)", "(Saint Budeaux Street)", "(Prince Rock Street)", "(Albercht Street)", "(GreenBank Street)"]
    fileprivate var datesArray = ["01@ - 07@", "09@ - 12@", "16@ - 18@", "23@ - 27@"]
    
    var placeType: ExpenseButtonTuple?
    var place: RestaurantTuple?
    var month: String?
    var period: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.setLeftBarButton(backBarButton(), animated: false)
        
        if titleView == nil {
            titleView = TitleView.getTitleView(for: place!.name, subtitle: period!, isSecondaryLableHidden: false)
        }
        
        navigationItem.titleView = titleView
        addAnnotations()
    }
    
    fileprivate func addAnnotations() {
        for coordinate in coordinateArray {
            let annotation = MapAnnotation(coordinate: CLLocationCoordinate2DMake(coordinate.lat, coordinate.long), name: place!.name, imageUrl: place!.image, reuseIdentifier: "mapAnnotation")
            placesMapView.addAnnotation(annotation)
        }
        
        
        let span = MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01)
        let region = MKCoordinateRegion(center: CLLocationCoordinate2DMake(51.509865, -0.118092), span: span)
        placesMapView.setRegion(region, animated: true)
    }
    
    func backBarButton() -> UIBarButtonItem {
        let reloadButton = UIButton(type: .custom)
        reloadButton.setTitle(" ", for: .normal)
        reloadButton.setImage(UIImage(named: "ic_arrow_back_white"), for: .normal)
        reloadButton.addTarget(self, action: #selector(onBack), for: .touchUpInside)
        reloadButton.sizeToFit()
        let customReloadBarButton = UIBarButtonItem(customView: reloadButton)
        
        return customReloadBarButton
    }
    
    func onBack() {
        navigationController?.popViewController(animated: true)
    }
}

extension ViewController2: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let viewC2 = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ViewController3") as! ViewController3
        viewC2.transitioningDelegate = self

        present(viewC2, animated: true, completion: nil)
    }
}

extension ViewController2: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return datesArray.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Details1Cell", for: indexPath) as! DetailsTableViewCell
        cell.typeImageView.image = UIImage(named: placeType!.image)
        cell.detailsNoOfVisitsLabel.text = placeType!.name
        cell.detailsTypeImageView.image = UIImage(named: place!.image)
        cell.detailsTypeLabel.text = place!.name + placesNamesArray[indexPath.row]
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let tableHeaderView = TableHeaderView.getHeaderView(for: datesArray[section].replacingOccurrences(of: "@", with: " \(month ?? "")"))
        
        return tableHeaderView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30
    }
}

extension ViewController2: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        guard annotation.isKind(of: MapAnnotation.self) else {
            return nil
        }
        
        let reuseIdentifier = "mapAnnotation"
        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: reuseIdentifier)
        
        if annotationView == nil {
            annotationView = MapAnnotationView(annotation: annotation, reuseIdentifier: reuseIdentifier)
            (annotationView as! MapAnnotationView).delegate = self
        } else {
            annotationView?.annotation = annotation as! MapAnnotation
        }
        
        return annotationView
    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        mapView.setCenter(view.annotation!.coordinate, animated: true)
    }
}

extension ViewController2: MapAnnotationViewDelegate {
    func mapAnnotationViewRequestsShowPlaceDetails(id: Int) {
        
    }
}

extension ViewController2: UIViewControllerTransitioningDelegate {
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return customPresentAnimationController
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return customDismissAnimationController
    }
}
