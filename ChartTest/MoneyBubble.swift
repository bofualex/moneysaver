//
//  MoneyBubble.swift
//  ChartTest
//
//  Created by Alex Bofu on 6/26/17.
//  Copyright © 2017 Alex Bofu. All rights reserved.
//

import UIKit

class MoneyBubble: UIView {

    @IBOutlet fileprivate var roundedButton: UIButton!
    @IBOutlet fileprivate var moneyLabel: UILabel!
    @IBOutlet fileprivate var decimalsLabel: UILabel!

    //MARK: - Public
    class func getSelectionMenuView(_ data: (day:String, value: Double)) -> MoneyBubble {
        let selectionMenuView: MoneyBubble = UIView.fromNib()
        selectionMenuView.roundedButton.layer.borderColor = UIColor.white.cgColor
    
        let text = String(format: "%.2f", arguments: [data.value])
        let stringArr = text.components(separatedBy: ".")
        
        selectionMenuView.moneyLabel.text = data.day + " - " + "£\(stringArr.first!)"
        selectionMenuView.decimalsLabel.text = ".\(stringArr.last!)"

        return selectionMenuView
    }
}
