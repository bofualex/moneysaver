//
//  DataGenerator.swift
//  ChartTest
//
//  Created by Alex Bofu on 6/25/17.
//  Copyright © 2017 Alex Bofu. All rights reserved.
//

import Foundation

class DataGenerator {
    
    static var randomizedSale: Double {
        return Double(arc4random_uniform(1000) + 1) / 10
    }
    
    static func addMonthsToDate(_ increment: Int) -> Date {
        let date = Date()
        let calendar = Calendar.current
        let year = calendar.component(.year, from: date)

        var dateComponent = DateComponents()
        dateComponent.month = 1
        dateComponent.day = 1
        dateComponent.year = year
        
        var dateComponentToAdd = DateComponents()
        dateComponentToAdd.month = increment
        
        let futureDate = Calendar.current.date(byAdding: dateComponentToAdd, to: Calendar.current.date(from: dateComponent)!)
        
        return futureDate!
    }

    static func data() -> Array<(month: String, Array<(day: String, value: Double)>)> {
        let calendar1 = Calendar.current
        var months = [String]()
        let formatter = DateFormatter()
        formatter.dateFormat = "MMM dd"

        var dataSource = Array<(month: String, Array<(day: String, value: Double)>)>()

        for month in calendar1.monthSymbols {
            months.append(month)
        }
        
        for i in 0..<months.count {
            let components = calendar1.dateComponents([.year, .month], from: addMonthsToDate(i))
            let startOfMonth = calendar1.date(from:components)!
            let numberOfDays = calendar1.range(of: .day, in: .month, for: startOfMonth)!.upperBound
            let allDays = Array(0..<numberOfDays).map{ calendar1.date(byAdding:.day, value: $0, to: startOfMonth)!}

            var values = Array<(day: String, value: Double)>()
            
            for day in allDays {
                let dateToDisplay = formatter.string(from: day)
                
                values.append((dateToDisplay, randomizedSale))
            }
            
            dataSource.append((months[i], values))
        }
        
        return dataSource
    }
}
