//
//  DetailsTableViewCell.swift
//  ChartTest
//
//  Created by Alex Bofu on 7/2/17.
//  Copyright © 2017 Alex Bofu. All rights reserved.
//

import UIKit

class DetailsTableViewCell: UITableViewCell {

    @IBOutlet weak var detailsTypeImageView: UIImageView!
    @IBOutlet weak var typeImageView: UIImageView!
    @IBOutlet weak var detailsTypeLabel: UILabel!
    @IBOutlet weak var detailsNoOfVisitsLabel: UILabel!
    @IBOutlet weak var detailsTotalLabel: UILabel!
    
    func setupCell(for expenseType: RestaurantTuple) {
        detailsTypeImageView.image = UIImage(named: expenseType.image)
        detailsNoOfVisitsLabel.text = "\(expenseType.count) visits"
        detailsTypeLabel.text = expenseType.name
    }
}
