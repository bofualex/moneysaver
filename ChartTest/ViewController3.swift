//
//  ViewController3.swift
//  ChartTest
//
//  Created by Alex Bofu on 7/3/17.
//  Copyright © 2017 Alex Bofu. All rights reserved.
//

import UIKit

class ViewController3: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func onBack() {
        dismiss(animated: true, completion: nil)
    }
}
