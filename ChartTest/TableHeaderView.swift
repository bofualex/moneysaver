//
//  TableHeaderView.swift
//  ChartTest
//
//  Created by Alex Bofu on 7/2/17.
//  Copyright © 2017 Alex Bofu. All rights reserved.
//

import UIKit

class TableHeaderView: UIView {

    @IBOutlet var titleLabel: UILabel!
    
    //MARK: - Public
    class func getHeaderView(for title: String) -> TableHeaderView {
        let titleView: TableHeaderView = UIView.fromNib()
        titleView.titleLabel.text = title
        
        return titleView
    }
}
