//
//  MapAnnotationView.swift
//  Berlins Best Coffee
//
//  Created by Alexandru Bofu on 31/03/2017.
//  Copyright © 2017 Alex Bofu. All rights reserved.
//

import UIKit
import MapKit

protocol MapAnnotationViewDelegate: class {
    func mapAnnotationViewRequestsShowPlaceDetails(id: Int)
}

final class MapAnnotationView: MKAnnotationView {
    
    weak var delegate: MapAnnotationViewDelegate?
    var calloutView: AnnotationCalloutView?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    override init(annotation: MKAnnotation?, reuseIdentifier: String?) {
        super.init(annotation: annotation, reuseIdentifier: reuseIdentifier)
        canShowCallout = false
        frame = CGRect(x: 0, y: 0, width: 70, height: 70)
        image = UIImage(named: "ic_place_white")?.withRenderingMode(.alwaysTemplate).colorized(color: UIColor(red:0.33, green:0.66, blue:1.00, alpha:1.0))
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: true)
        
//        if selected {
//            calloutView = AnnotationCalloutView.getCalloutView(for: (annotation as? MapAnnotation)!)
//            calloutView?.delegate = self
//            calloutView?.frame = CGRect(x: -UIScreen.main.bounds.width / 3 + bounds.width / 2, y: -(UIScreen.main.bounds.width * 2 / 9) - 5, width: 120, height: 40)
//            addSubview(calloutView!)
//        } else {
//            calloutView?.removeFromSuperview()
//        }
    }
    
    override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        let hitView = super.hitTest(point, with: event)
        
        if hitView != nil {
            self.superview?.bringSubview(toFront: self)
        }
        
        return hitView
    }
    
    override func point(inside point: CGPoint, with event: UIEvent?) -> Bool {
        let rect = self.bounds
        var isInside: Bool = rect.contains(point)
        
        if !isInside {
            for view in self.subviews {
                isInside = view.frame.contains(point)
                
                if isInside {
                    break
                }
            }
        }
        
        return isInside
    }
}

extension MapAnnotationView: AnnotationCalloutViewDelegate {
    func annotationCalloutViewRequestsShowPlaceDetails(id: Int) {
        delegate?.mapAnnotationViewRequestsShowPlaceDetails(id: id)
    }
}
