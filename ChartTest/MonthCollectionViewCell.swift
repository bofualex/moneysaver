//
//  MonthCollectionViewCell.swift
//  ChartTest
//
//  Created by Alex Bofu on 6/26/17.
//  Copyright © 2017 Alex Bofu. All rights reserved.
//

import UIKit

class MonthCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var monthButton: CollectionViewButton!
    
    override var isSelected: Bool {
        get {
            return super.isSelected
        }
        set {
            if newValue {
                super.isSelected = true
                monthButton.backgroundColor = .white
                monthButton.setTitleColor(UIColor(red:0.33, green:0.66, blue:1.00, alpha:1.0), for: .normal)
            } else if newValue == false {
                super.isSelected = false
                monthButton.backgroundColor = .clear
                monthButton.setTitleColor(.white, for: .normal)
            }
        }
    }

    func setupCell(for month: String) {
        monthButton.setTitle(month, for: .normal)
    }
}
