//
//  MapAnnotation.swift
//  Berlins Best Coffee
//
//  Created by Alexandru Bofu on 31/03/2017.
//  Copyright © 2017 Alex Bofu. All rights reserved.
//

import Foundation
import MapKit

final class MapAnnotation: NSObject, MKAnnotation {
    
    var coordinate: CLLocationCoordinate2D
    var name: String?
    var imageUrl: String?
    var reuseIdentifier: String
    
    init(coordinate: CLLocationCoordinate2D, name: String?, imageUrl: String?, reuseIdentifier: String) {
        self.coordinate = coordinate
        self.name = name
        self.imageUrl = imageUrl
        self.reuseIdentifier = reuseIdentifier
    }
}
