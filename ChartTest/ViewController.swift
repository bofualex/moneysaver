//
//  ViewController.swift
//  ChartTest
//
//  Created by Alex Bofu on 6/25/17.
//  Copyright © 2017 Alex Bofu. All rights reserved.
//

import UIKit
import Charts
import FLKAutoLayout

typealias ExpenseButtonTuple = (name: String, image: String)
typealias RestaurantTuple = (name: String, image: String, count: Int)

class ViewController : UIViewController {
    
    @IBOutlet weak var typesTable: UITableView!
    @IBOutlet weak var monthsCollectionView: UICollectionView!
    @IBOutlet weak var lineChart: LineChartView!
    @IBOutlet weak var periodLabel: UILabel!
    @IBOutlet weak var totalMonthlyExpensesLabel: UILabel!
    @IBOutlet weak var totalMonthlyExpensesDecimalLabel: UILabel!
    @IBOutlet weak var selectedTypeLabel: UILabel!
    @IBOutlet weak var selectedTypeImageView: UIImageView!
    @IBOutlet weak var selectedTypeContainerView: UIView!
    
    fileprivate var tableHeaderView: TableHeaderView?
    fileprivate var moneyView: MoneyBubble?
    fileprivate var titleView: TitleView?
    let customNavigationAnimationController = CustomNavigationAnimationController()

    fileprivate lazy var monthButtonsArray: [UIButton] = []
    fileprivate let sales = DataGenerator.data()
    fileprivate var selectedIndex = 0
    fileprivate var isInitialLoad = true
    fileprivate var isShowingMonthlyExpenses = false
    fileprivate var scrollIndex: IndexPath?
    fileprivate var selectedChartValues: LineChartDataSet?
    fileprivate let tableButtonsArray: [ExpenseButtonTuple] = [("Restaurants", "burger"), ("Shopping", "shopping-basket"), ("Travel", "suitcase"), ("Bills", "invoice"), ("Fun", "cocktail")]
    fileprivate let restaurantsArray: [RestaurantTuple] = [("Starbucks", "Starbucks", 8), ("KFC", "KFC", 9), ("Taco's", "tacos", 3), ("McDonalds", "mcdonalds", 5), ("Pizza Express", "PizzaExpress", 12), ("Burger King", "burgerking", 8), ("Pizza Hut", "pizzahut", 7), ("Bella Italia", "bella", 4), ("Subway", "subway", 6)]
    
    var progress: Double = 0
    var timer: Timer?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if isShowingMonthlyExpenses {
            navigationItem.setLeftBarButton(backBarButton(), animated: false)
        } else {
            navigationItem.setLeftBarButton(drawerBarButton(), animated: false)
        }

        timer = Timer.scheduledTimer(timeInterval: 0.01, target:self, selector: #selector(updateProgress), userInfo: nil, repeats: true)
    }
    
    override func viewDidLoad() {
        if titleView == nil {
            titleView = TitleView.getTitleView(for: "Spending overview")
        }
        
        tableHeaderView = TableHeaderView.getHeaderView(for: "")
        navigationItem.titleView = titleView
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) { [weak self] in
            if self!.isInitialLoad {
                let calendar = Calendar.current
                let month = calendar.component(.month, from: Date())
                var dateComponent = DateComponents()
                dateComponent.month = month
                
                self?.scrollIndex = IndexPath(row: month - 1, section: 0)
                self!.isInitialLoad = !self!.isInitialLoad
            }
            
            if self?.scrollIndex != nil {
                self?.monthsCollectionView.selectItem(at: self!.scrollIndex!, animated: true, scrollPosition: .centeredHorizontally)
                self?.onMonth(self!.scrollIndex!.row)
                self?.selectedIndex = self!.scrollIndex!.row
            }
        }
        
        lineChart.delegate = self
        lineChart.chartDescription?.text = ""
        lineChart.xAxis.drawLabelsEnabled = false
        lineChart.leftAxis.labelTextColor = .white
        lineChart.leftAxis.labelFont = UIFont.systemFont(ofSize: 14)
        lineChart.leftAxis.axisMinimum = -40
        lineChart.leftAxis.axisMaximum = 100
        lineChart.leftAxis.drawZeroLineEnabled = false
        lineChart.leftAxis.drawBottomYLabelEntryEnabled = false
        lineChart.rightAxis.enabled = false
        lineChart.xAxis.drawGridLinesEnabled = false
        lineChart.legend.enabled = false
        lineChart.pinchZoomEnabled = true
        lineChart.doubleTapToZoomEnabled = true
    }
    
    func onMonth(_ index: Int) {
        if isShowingMonthlyExpenses {
            onBack()
        }
        
        if moneyView != nil {
            moneyView?.removeFromSuperview()
            moneyView = nil
        }
        
        selectedIndex = index
        var salesEntries = [ChartDataEntry]()
        var i = 0
        let dailyValues = sales[index].1
        var total = 0.0
        
        for sale in dailyValues {
            let saleEntry = ChartDataEntry(x: Double(i), y: sale.value)
            salesEntries.append(saleEntry)
            i += 1
            total += sale.value
         }
        
        periodLabel.text = dailyValues.first!.day + " - " + dailyValues[dailyValues.count - 2].day
        let string = String(format: "%.2f", arguments: [total])
        let stringArr = string.components(separatedBy: ".")
        totalMonthlyExpensesLabel.text = "£\(stringArr.first!)"
        totalMonthlyExpensesDecimalLabel.text = ".\(stringArr.last!)"
        
        let chartDataSet = LineChartDataSet(values: salesEntries, label: "")
        chartDataSet.drawCirclesEnabled = false
        let gradientColors = [UIColor(red:0.33, green:0.66, blue:1.00, alpha:1.0).cgColor, UIColor(red:0.33, green:0.66, blue:1.00, alpha:1.0).cgColor, UIColor(red:0.18, green:0.24, blue:0.36, alpha:1.0).cgColor] as CFArray
        let colorLocations:[CGFloat] = [1, 0.4, 0]
        let gradient = CGGradient(colorsSpace: CGColorSpaceCreateDeviceRGB(), colors: gradientColors, locations: colorLocations)
        chartDataSet.fill = Fill.fillWithLinearGradient(gradient!, angle: 90)
        chartDataSet.drawFilledEnabled = true
        chartDataSet.mode = .horizontalBezier
        
        if selectedChartValues != nil {
            
//            let chartData = LineChartData(dataSets: [animateDataChange(selectedChartValues!, toDataSet: chartDataSet, progress: 0.5)])
//            let chartData = LineChartData(dataSet: chartDataSet)
//            chartData.setDrawValues(false)
//            lineChart.data = chartData
//            lineChart.autoScaleMinMaxEnabled = true
        }
        
        
        let chartData = LineChartData(dataSet: chartDataSet)
        chartData.setDrawValues(false)
        lineChart.data = chartData
//        lineChart.autoScaleMinMaxEnabled = true
//        selectedChartValues = chartDataSet
        lineChart.animate(yAxisDuration: 0.3, easingOption: .easeInCubic)
    }
    
    func updateProgress() {
        guard progress <= 1 else {
            timer?.invalidate()
            return
        }
        
        progress += 0.01
    }
    
    func animateDataChange(_ fromDataSet: LineChartDataSet, toDataSet: LineChartDataSet, progress: Double) -> LineChartDataSet {
        var dataEntries: [ChartDataEntry] = []
        
        for i in 0..<fromDataSet.values.count {
            let yValue = (toDataSet.values[i].y - fromDataSet.values[i].y) * progress + fromDataSet.values[i].y
            let dataEntry = ChartDataEntry(x: Double(i), y: yValue)
            dataEntries.append(dataEntry)
        }
        
        let animatedDataSet = LineChartDataSet(values: dataEntries, label: "")
        
        return animatedDataSet
    }
    
    func showMoneyDetails(for point: CGPoint, index: Int) {
        if moneyView != nil {
            moneyView?.removeFromSuperview()
            moneyView = nil
        }
        
        moneyView = MoneyBubble.getSelectionMenuView(sales[selectedIndex].1[index])
        moneyView?.frame = CGRect(x: point.x - 50, y: point.y - 35, width: 105, height: 40)
        view.addSubview(moneyView!)
    }
    
    func drawerBarButton() -> UIBarButtonItem {
        let reloadButton = UIButton(type: .custom)
        reloadButton.setTitle(" ", for: .normal)
        reloadButton.setImage(UIImage(named: "ic_menu_white"), for: .normal)
        reloadButton.sizeToFit()
        let customReloadBarButton = UIBarButtonItem(customView: reloadButton)
        
        return customReloadBarButton
    }
    
    func searchBarButton() -> UIBarButtonItem {
        let reloadButton = UIButton(type: .custom)
        reloadButton.setTitle(" ", for: .normal)
        reloadButton.setImage(UIImage(named: "ic_search_white"), for: .normal)
        reloadButton.sizeToFit()
        let customReloadBarButton = UIBarButtonItem(customView: reloadButton)
        
        return customReloadBarButton
    }

    func backBarButton() -> UIBarButtonItem {
        let reloadButton = UIButton(type: .custom)
        reloadButton.setTitle(" ", for: .normal)
        reloadButton.setImage(UIImage(named: "ic_arrow_back_white"), for: .normal)
        reloadButton.addTarget(self, action: #selector(onBack), for: .touchUpInside)
        reloadButton.sizeToFit()
        let customReloadBarButton = UIBarButtonItem(customView: reloadButton)
        
        return customReloadBarButton
    }
    
    func clearBarButton() -> UIBarButtonItem {
        let reloadButton = UIButton(type: .custom)
        reloadButton.setTitle("", for: .normal)
        let customReloadBarButton = UIBarButtonItem(customView: reloadButton)
        
        return customReloadBarButton
    }

    func onBack() {
        titleView?.titleLabel.text = "Spending overview"
        navigationItem.setLeftBarButton(drawerBarButton(), animated: false)
        navigationItem.setRightBarButton(clearBarButton(), animated: false)
        isShowingMonthlyExpenses = false
        onMonth(monthsCollectionView.indexPathsForSelectedItems!.first!.row)
        typesTable.reloadData()
        
        UIView.animate(withDuration: 0.3, animations: { [weak self] in
            self?.selectedTypeContainerView.alpha = 0
        })
    }
}

extension ViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        onMonth(indexPath.row)
        collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
    }
}

extension ViewController: UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return sales.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MonthCell", for: indexPath) as! MonthCollectionViewCell
        cell.setupCell(for: sales[indexPath.row].month)
        
        return cell
    }
}

extension ViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: sales[indexPath.row].month.width(withConstrainedHeght: 23, font: UIFont.systemFont(ofSize: 15)) + 12, height: 50)
    }
}

extension ViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard !isShowingMonthlyExpenses else {
            let viewC2 = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ViewController2") as! ViewController2
            viewC2.placeType = tableButtonsArray.first!
            viewC2.place = restaurantsArray[indexPath.row]
            viewC2.month = sales[monthsCollectionView.indexPathsForSelectedItems!.first!.row].month
            viewC2.period = periodLabel.text
            navigationController?.delegate = self
            navigationController?.pushViewController(viewC2, animated: true)
            
            return
        }
        
        navigationItem.setLeftBarButton(backBarButton(), animated: false)
        navigationItem.setRightBarButton(searchBarButton(), animated: false)
        isShowingMonthlyExpenses = true
        selectedTypeImageView.image  = UIImage(named: tableButtonsArray[indexPath.row].image)
        selectedTypeLabel.text = tableButtonsArray[indexPath.row].name
        titleView?.titleLabel.text = tableButtonsArray[indexPath.row].name
        tableView.reloadData()
        
        UIView.animate(withDuration: 0.3, animations: { [weak self] in
            self?.selectedTypeContainerView.alpha = 1
        })
    }
}

extension ViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isShowingMonthlyExpenses {
            return 9
        } else {
            return 5
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell: UITableViewCell!
        
        if isShowingMonthlyExpenses {
            cell = tableView.dequeueReusableCell(withIdentifier: "DetailsCell", for: indexPath) as! DetailsTableViewCell
            (cell as! DetailsTableViewCell).setupCell(for: restaurantsArray[indexPath.row])
        } else {
            cell = tableView.dequeueReusableCell(withIdentifier: "MoneyCell", for: indexPath) as! MoneyTableViewCell
            (cell as! MoneyTableViewCell).setupCell(for: tableButtonsArray[indexPath.row])
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return tableHeaderView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if isShowingMonthlyExpenses {
            let dailyValues = sales[monthsCollectionView.indexPathsForSelectedItems!.first!.row].1
            tableHeaderView?.titleLabel.text = dailyValues.first!.day + " - " + dailyValues[dailyValues.count - 2].day

            return 40
        } else {
            return 0
        }
    }
}

extension ViewController: ChartViewDelegate {
    func chartValueSelected(_ chartView: ChartViewBase, entry: ChartDataEntry, highlight: Highlight) {
        let point: CGPoint = CGPoint(x: highlight.xPx, y: highlight.yPx)
        let point1 = chartView.convert(point, to: view)
        showMoneyDetails(for: point1, index: Int(highlight.x))
        let emptyVals = [Highlight]()
        chartView.highlightValues(emptyVals)
    }
    
    func chartValueNothingSelected(_ chartView: ChartViewBase) {
        if moneyView != nil {
            moneyView?.removeFromSuperview()
            moneyView = nil
        }
    }
}

extension ViewController: UINavigationControllerDelegate {
    func navigationController(_ navigationController: UINavigationController, animationControllerFor operation: UINavigationControllerOperation, from fromVC: UIViewController, to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        customNavigationAnimationController.reverse = operation == .pop
        
        return customNavigationAnimationController
    }
}
