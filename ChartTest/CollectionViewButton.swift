//
//  CollectionViewButton.swift
//  ChartTest
//
//  Created by Alex Bofu on 6/26/17.
//  Copyright © 2017 Alex Bofu. All rights reserved.
//

import UIKit

class CollectionViewButton: UIButton {

    override init(frame:CGRect) {
        super.init(frame:frame)

        defaultInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        defaultInit()
    }
    
    override var isHighlighted: Bool {
        get {
            return super.isHighlighted
        }
        set {
            if newValue {
                super.isHighlighted = true
            } else if newValue == false {
                super.isHighlighted = false
            }
        }
    }

    fileprivate func defaultInit() {
        self.backgroundColor = .clear
        self.setTitleColor(.white, for: .normal)
        self.clipsToBounds = true
        self.layer.cornerRadius = 10
        
        self.titleLabel?.numberOfLines = 1
        self.titleLabel?.adjustsFontSizeToFitWidth = false
        self.titleLabel?.lineBreakMode = .byClipping
        
        let titleInsets: UIEdgeInsets = UIEdgeInsetsMake(0, 6, 0, -6)
        var contentInsets = UIEdgeInsetsMake(6, 0, 6, 0)
        let extraWidthRequiredForTitle = titleInsets.left - titleInsets.right
        contentInsets.right += extraWidthRequiredForTitle
        self.titleEdgeInsets = titleInsets
        self.contentEdgeInsets = contentInsets
        self.sizeToFit()
    }
}
