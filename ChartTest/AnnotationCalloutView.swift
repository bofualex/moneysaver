//
//  AnnotationCalloutView.swift
//  Berlins Best Coffee
//
//  Created by Alexandru Bofu on 31/03/2017.
//  Copyright © 2017 Alex Bofu. All rights reserved.
//

import UIKit

protocol AnnotationCalloutViewDelegate: class {
    func annotationCalloutViewRequestsShowPlaceDetails(id: Int)
}

final class AnnotationCalloutView: UIView {

    @IBOutlet weak var placeNameLabel: UILabel!
    @IBOutlet weak var placeImageView: UIImageView!
    
    weak var delegate: AnnotationCalloutViewDelegate?
    var annotation: MapAnnotation?
    
    //MARK: - memory management
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    //MARK: - private
    fileprivate func setupUI() {
        placeNameLabel.text = annotation?.name
        placeImageView.image = UIImage(named: "Starbucks")
    }
    
    //MARK: - actions
    @IBAction func onShowPlaceDetails() {
//        delegate?.annotationCalloutViewRequestsShowPlaceDetails(id: annotation!.id)
    }
    
    //MARK: - public
    static func getCalloutView(for annotation: MapAnnotation) -> AnnotationCalloutView {
        let calloutView = AnnotationCalloutView.instanceFromNib() as! AnnotationCalloutView
        calloutView.annotation = annotation
        calloutView.setupUI()
        
        return calloutView
    }
    
    class func instanceFromNib() -> UIView {
        return UINib(nibName: "AnnotationCalloutView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! UIView
    }
    
    override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        let hitView = super.hitTest(point, with: event)
        
        if hitView != nil {
            self.superview?.bringSubview(toFront: self)
        }
        
        return hitView
    }
    
    override func point(inside point: CGPoint, with event: UIEvent?) -> Bool {
        let rect = self.bounds
        var isInside: Bool = rect.contains(point)
        
        if !isInside {
            for view in self.subviews {
                isInside = view.frame.contains(point)
                
                if isInside {
                    break
                }
            }
        }
        
        return isInside
    }
}
